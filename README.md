# Nicomath

Mathearbeitsblätter für den Nico

------

This is a - work in progress - personal project which follows a number of goals:

 * Being a math worksheet generator for my son
 * Being a learning platform to learn and try out technologies
   * Kotlin
   * ~~vue.js frontend programming~~
   * react.js (functional) frontend programming
 * Being a coding showcase to third parties
  
   
## Worksheet generator

The first goal has been solved in December with the template
based (static HTML) version being available back in the 
git log. I've still got that version running on my laptop.

Version 2 is the current version, as time allows I'm targeting
a web based Minimum Viable Product that can be used by anyone
on the internet.

## Learning + Experimentation platform

In my day-by-day business I typically use Spring Boot (currently 
Spring Boot 2), modern Java, JPA, Spring Security etc. etc.
with a fullstack but "mostly backend" focus.

As I'm curious to try out new technology this started out with
Kotlin+Ktor and currently turns into having a large
frontend part, so I can flex my muscles in that area.

This project ultimately also targets a CI pipeline, currently
build is triggered via gradle, CI runs on Bitbucket Pipelines.
Open goal is to completely build a deployable docker container
from the main gradle file.

## Showcase

This also serves to showcase some of my source code to third
parties who requested that. If you are such a third party please
not two important issues:

 * This is an experimental prototype, aggressiveley using unusual 
   technologies. For customer projects I select frameworks a 
   little more conservatively, depending on community size and
   expected support level in the future.
 * This contains a large part frontend programming and rather
   no backend/database at all - in commercial work things are
   different.  
    