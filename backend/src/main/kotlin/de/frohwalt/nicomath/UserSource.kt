package de.frohwalt.nicomath

import io.ktor.auth.UserPasswordCredential

interface UserSource {

    fun findUserById(id: Int): User

    fun findUserByCredentials(credential: UserPasswordCredential): User

}