package de.frohwalt.nicomath

data class ExerciseDto(val id: Int, val seed: Long, val type: String, val steps: List<Any>)

data class WorksheetDto(val id: Int, val exercises: List<ExerciseDto>)