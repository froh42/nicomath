package de.frohwalt.nicomath

import org.dizitart.kno2.nitrite
import java.io.File

val database = nitrite {
    file = File("nicomath.db")       // or, path = fileName
    autoCommitBufferSize = 2048
    compress = true
    autoCompact = false
}