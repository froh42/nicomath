package de.frohwalt.nicomath

import io.ktor.auth.Principal

data class User(
    val id: Int,
    val name: String,
    val countries: List<String>
) : Principal