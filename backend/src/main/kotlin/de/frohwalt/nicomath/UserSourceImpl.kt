package de.frohwalt.nicomath

import io.ktor.auth.UserPasswordCredential
import me.avo.io.ktor.auth.jwt.sample.testUser

class UserSourceImpl : UserSource {

    override fun findUserById(id: Int): User = users.getValue(id)

    override fun findUserByCredentials(credential: UserPasswordCredential): User = testUser

    private val users = listOf(testUser).associateBy(User::id)

}