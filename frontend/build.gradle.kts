import com.moowork.gradle.node.npm.NpmTask

plugins {
    base
    id("com.moowork.node") version "1.3.1"
}

buildscript {
    repositories {
        jcenter()
    }

    dependencies {
        classpath("org.jetbrains.kotlin:kotlin-gradle-plugin:1.3.21")
    }
}

node {
    download = true
}

repositories {
    maven("http://repo.maven.apache.org/maven2")
}

configurations {
    create("frontend")
}

val npmBuild = task<NpmTask>("npmBuild") {
    dependsOn("npm_install")
    setArgs(listOf("run", "build"))
}

val packageApp = task<Zip>("packageApp") {
    //archiveFileName.set("frontend-${project.version}.zip")
    dependsOn("npmBuild")
    archiveBaseName.set("frontend")
    archiveExtension.set("jar")
    archiveVersion.set(project.version.toString())
    from("dist/")
    include("**")
    into("static")
}

task<Delete>("cleanup") {
    delete("build", "dist")
}

tasks.getByName("assemble").dependsOn(npmBuild)

artifacts {
    add("frontend", packageApp)
}

