# Main Application

Contains the main application.


### Global controls

Contain global functions like "clear"

### Exercises

Each exercise contains a number of repeated
tasks akin to a school book were we have exercise 
1. with subtask a) b) c) and so on.

Nomenclature:

* Exercises are top level 
* tasks are repetitions of an exercises

NOT IN THE MVP: Edit or delete an existing exercise

### Editing controls

Manages adding a new exercise 

# Implementation strategy:

Since my current focus shifts from trying kotlin 
I'll be moving exercise generation from
the backend completely into the frontend.

The backend's job will be authentication,
authorization and storage.

