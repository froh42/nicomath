import {useCookie} from "@use-hook/use-cookie";
import "bulma/css/bulma.css";
import React, {useEffect, useState} from "react";
import EditExercise from "../editExercise/EditExercise";
import Login from "../login/Login";
import {makeExercise} from "../model/model";
import {Banner} from "./Banner";
import {AuthenticationContext, getAuthenticationContext} from "./AuthenticationContext";

// Currently constant, need to query from available Renderer in the Future after we
// move away from dummyRenderers to real exercise Map.
const availableTypes = {
    addition: "Addition",
    subtraction: "Subtraktion",
    multiplication: "Multiplikation",
    division: "Divison"
};

function fakeGetFromServer(setExercise) {
    // Set data after 400ms
    // MVP App does not prevent from adding something in the meantime.
    setTimeout(() => setExercise([makeExercise("addition")]), 400);
}

function fakeWriteBack(exercises) {
    // console.log("Fake write back data to backend");
    // console.log(exercises);
}


function App() {
    // noinspection JSUnusedLocalSymbols
    const [exercises, setExercise] = useState([]);
    const [authentication, setAuthentication] = useCookie("auth_token", undefined);
    const authenticationContext = getAuthenticationContext(authentication, setAuthentication);
    useEffect(() => fakeGetFromServer(setExercise), []);
    useEffect(() => fakeWriteBack(exercises), [exercises]);
    const addExercise = type => setExercise([...exercises, makeExercise(type)]);

    const topControl = authenticationContext.isLoggedIn() ?
        <EditExercise exercises={exercises}
                      availableTypes={availableTypes}
                      addExercise={addExercise}
        /> :
        <Login user="test" onLogin={authenticationContext.login}/>;
    return (
        <AuthenticationContext.Provider value={authenticationContext}>
            <Banner onLogout={() => alert("Mööpf")}/>
            {topControl}
        </AuthenticationContext.Provider>
    );
}

export default App;

//  https://codepen.io/froh42/project/editor/DORawR
//  https://jesobreira.github.io/Bulma-Form-Builder/ Auswahlseite mit Drag & Drop
//  https://dansup.github.io/bulma-templates/
//  https://dansup.github.io/bulma-templates/templates/kanban[search].html bulma with vue kanban sample

// Oben auf dem Blatt gibt es ein Eingabenfeld für den aktuellen Seed mit "Würfel-Button". Durch den
// Seed bestimmen sich die Aufgaben, durch Neueingabe des Seed (auch durch URL steuerbar - QR-Code!
// https://davidshimjs.github.io/qrcodejs/) kann ich den Inhalt eines existierenden Arbeitsblatt anzeigen.
// (AHa - der QR-Code muss also alle Parameter enthalten. Und wenn sich der Algorithmus ändert, werden
// alte QR ungültig - ggf. Datenbank mit den Aufgabenständen?)
//
// MVP: Kein Seedfeeld.
//
// Musterarbeitsblatt wird dargestellt mit "+" Button.
// Nach dem Klick auf den "+" Button kann ich die nächste Aufgabenart auswählen, die Anzahl und die
// ggf. die Aufgabenparameter (Schwierigkeitsgrad). Nach Absenden taucht an dieser Stelle Aufgabenblock auf.
//
// Existierende Sektionen werden mit einem "-" dargestellt um die Sektion zu entfernen
//
// Unten: Wenn ein Arbeitsblatt fertig ist, kann ich es als Vorlage speichern (später: Zu meinem User-Account)
// MVP: Link auf Seite, die ich im Browser mit Bookmark merken kann, es wird nicht serverseitig
// gespeichert.
//
