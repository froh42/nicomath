import React from "react";
import axios from "axios";

export const AuthenticationContext = React.createContext();

export function getAuthenticationContext(authentication, setAuthentication) {
    return {
        authentication: authentication,
        logout: () => setAuthentication(null, {}),
        login: (u, pw) => {
            return axios.post("/api/login", {
                headers: {
                    "Content-Type": "application/json"
                },
                body: {
                    name: u,
                    password: pw
                }
            }).then(r => {
                console.log("returned", r.data);
                setAuthentication(r.data, {});
                console.log("auth", authentication);

            });
        },
        isLoggedIn: () => authentication != null
    };
}