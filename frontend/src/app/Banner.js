import {
    Button,
    Control,
    Field,
    Navbar,
    NavbarBrand,
    NavbarDivider,
    NavbarDropdown,
    NavbarEnd,
    NavbarItem,
    NavbarLink,
    NavbarMenu,
    NavbarStart
} from "bloomer";
import React, {useContext} from "react";
import {AuthenticationContext} from "./AuthenticationContext";

function MyNavbar() {
    const authenticationContext = useContext(AuthenticationContext);
    return authenticationContext.isLoggedIn() ? <React.Fragment>
            <NavbarStart>
                <NavbarItem hasDropdown isHoverable>
                    <NavbarLink href='#/documentation'>Documentation</NavbarLink>
                    <NavbarDropdown>
                        <NavbarItem href='#/'>One A</NavbarItem>
                        <NavbarItem href='#/'>Two B</NavbarItem>
                        <NavbarDivider/>
                        <NavbarItem href='#/'>Two A</NavbarItem>
                    </NavbarDropdown>
                </NavbarItem>
            </NavbarStart>
            <NavbarEnd>
                <NavbarItem>
                    <Field isGrouped>
                        <Control>
                            <Button onClick={() => authenticationContext.logout()} id="logout">
                                <span>Logout</span>
                            </Button>
                        </Control>
                    </Field>
                </NavbarItem>
            </NavbarEnd>
        </React.Fragment>
        : <React.Fragment/>;
}

export const Banner = () => {
    //const authenticationContext = useContext(AuthenticationContext);
    return <Navbar>
        <NavbarBrand>
            <NavbarItem>
                Nicomath
            </NavbarItem>
        </NavbarBrand>
        <NavbarMenu isActive={true}>
            <MyNavbar/>

        </NavbarMenu>
    </Navbar>
};
