import {Button, Container, Section} from "bloomer";
import React from "react";

const EditorControls = () =>
    <Section>
        <Container>
            <div className="is-pulled-right">
                        <span>
                            <Button>clear</Button>
                        </span>
            </div>
        </Container>
    </Section>;

export default EditorControls;