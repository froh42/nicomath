import React from "react";
import ReactDOM from "react-dom";
import EditExercise from "./EditExercise";

it("renders without crashing", () => {
    const div = document.createElement("div");
    ReactDOM.render(<EditExercise/>, div);
    ReactDOM.unmountComponentAtNode(div);
});
