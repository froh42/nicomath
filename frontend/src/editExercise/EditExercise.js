import React from "react";
import ExerciseSection from "./../exercise/ExerciseSection";
import EditorControls from "./EditorControls";


const EditExercise = props =>
    <div>
        <EditorControls/>
        <ExerciseSection exercises={props.exercises}/>
        {/*<ExerciseAdder availableTypes={props.availableTypes}*/}
        {/*               addExercise={props.addExercise}/>*/}
    </div>;


export default EditExercise;
