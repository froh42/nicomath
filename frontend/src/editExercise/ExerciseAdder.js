import {Button, Container, Icon, Section} from "bloomer";
import React, {useState} from "react";

const ExerciseAdder = props => {
    const [editing, setEditing] = useState(false);
    const initialSelected = "init";
    const [selected, setSelected] = useState(initialSelected);

    const addNewExercise = () => {
        const newExerciseType = selected;
        setSelected(initialSelected);
        setEditing(false);
        props.addExercise(newExerciseType);
    };

    if (!editing) {
        return <Section>
            <Container>
                <Button size="large" onClick={() => setEditing(true)}>
                    <Icon size="large"><i className="fas fa-plus"/></Icon>
                    <span>Neue Aufgabe</span>
                </Button>
                <div id="newExerciseEnd"/>
            </Container>
        </Section>;

    } else {
        const options = Object.keys(props.availableTypes)
            .map((k, i) => <option key={i} value={k}>{props.availableTypes[k]}</option>);


        return <Section>
            <Container>
                <div>
                    <label>
                        <select defaultValue={initialSelected} onChange={ev => setSelected(ev.target.value)}>
                            <option disabled value={initialSelected}>Bitte Aufgabentyp auswählen ...</option>
                            {options}
                        </select>
                    </label>
                </div>
                <div>
                    <span>Debug: Selected {selected}</span>
                </div>
                <Button disabled={selected === initialSelected} onClick={addNewExercise}>
                    <span>Hinzufügen!</span>
                </Button>

            </Container>
            <div id="newExerciseEnd"/>
        </Section>;
    }
};

export default ExerciseAdder;