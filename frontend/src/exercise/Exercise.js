import {Columns, Container, Section} from "bloomer";
import React from "react";

// Simple placeholder tasks
//
// Later on we'll move those to their own components which are
//
//   - React components rendering the subtask with SVG
//   - With and without result (and possibly a conigurable hint level)
//   - Also contain an exercise generator, which will create a random exercise
//
// Previously the idea was to use a seed for fully-random exercises, but we'd better
// have the exercises have their own random exercise generator which takes a difficulty
// level. Steps will be able to create a memento for storage. This memento will be
// a leaf in the global app state so it can be stored/retrieved from the server.
//
const dummyRenderers = {
    addition: m => <span>Addiere: {m.a} + {m.b} = ___</span>,
    subtraction: m => <span>Addiere: {m.a} - {m.b} = ___</span>,
    multiplication: m => <span>Multipliziere: {m.a} * {m.b} = ___</span>,
    division: m => <span>Dividiere: {m.a} : {m.b} = ___</span>

};

const getRenderer = r => dummyRenderers[r] || (() => <span>Unknown</span>);

export const Exercise = exercise =>
    <div>
        <Section>
            <Container>
                <h1 className="aufgabe title">{exercise.num}. {exercise.title}</h1>
                <p className="subtitle">{exercise.subtitle}</p>
                <Columns multiline breakpoint="desktop">
                    {
                        exercise.tasks.map((task, i) =>
                            <Columns.Column
                                key={i}
                                size="one-quarter">{getRenderer(task.type)(task.data)} </Columns.Column>)
                    }
                </Columns>
            </Container>
        </Section>
    </div>;