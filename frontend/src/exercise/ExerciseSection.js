import {Section} from "bloomer";
import React from "react";

const ExerciseSection = props =>
    <Section>
        {props.exercises.map((exProps, i) =>
            <div key={i} {...exProps} num={i + 1}>{i}</div>)}
    </Section>;

export default ExerciseSection;