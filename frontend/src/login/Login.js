import {Button, Container, Control, Field, Input, Label, Section, Title} from "bloomer";
import React, {useState} from "react";


const Login = props => {
    const [user, setUser] = useState(props.user || "");
    const [password, setPassword] = useState("aa");

    const onSubmit = (e) => {
        if (props.onLogin !== undefined) {
            props.onLogin(user, password);
        }
        e.preventDefault();
    };

    return <Section>
        <Container>
            <form onSubmit={onSubmit}>
                <Title>Login</Title>
                <Field>
                    <Label>Name</Label>
                    <Control>
                        <Input value={user} type="text" placeholder='Text Input'
                               onChange={ev => setUser(ev.target.value)}/>
                    </Control>
                </Field>
                <Field>
                    <Label>Password</Label>
                    <Control>
                        <Input value={password} type="text" placeholder='Password Input'
                               onChange={ev => setPassword(ev.target.value)}/>
                    </Control>
                </Field>
                <Button disabled={user === "" || password === ""} type="submit">Login</Button>
            </form>
        </Container>
    </Section>;
};

export default Login;
