// As of now this creates just a few factory functions mushed together in one
// file, see README.md for furhter toughts on concepts and refactoring.
// For prototype state this is "good enough".

// https://stackoverflow.com/a/1527820/60229
function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function makeTasks(type) {
    return [...Array(8).keys()].map(i =>
        ({
            num: i,
            type: type,
            data: ({a: getRandomInt(1, 10), b: getRandomInt(1, 10)})
        }));
}

export function makeExercise(type = "addition") {
    switch (type) {
        case "addition":
            return {
                title: "Addiere zwei Zahlen",
                subtitle: "Zähle die Zahlen zusammen",
                tasks: makeTasks(type)
            };
        case "subtraction":
            return {
                title: "Subtrahiere zwei Zahlen",
                subtitle: "Ziehe die Zahlen voneinander ab",
                tasks: makeTasks(type)
            };
        case "multiplication":
            return {
                title: "Multipliziere zwei Zahlen",
                subtitle: "Berechnes das Produkt",
                tasks: makeTasks(type)
            };
        case "division":
            return {
                title: "Dividiere zwei Zahlen",
                subtitle: "Berechne den Quotienten",
                tasks: makeTasks(type)
            };
        default:
            throw new Error("Illegal exercise type");
    }
}
