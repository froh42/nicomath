allprojects {
    group = "de.frohwalt.nicomath"
    version = "0.0.3-SNAPSHOT"
}



tasks {
    "wrapper"(Wrapper::class) {
        distributionType = Wrapper.DistributionType.ALL
    }
}